Sql语句

CREATE TABLE BookCategory (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category_name VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE BookInfo (
    book_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL,
    price DECIMAL(10,2) CHECK (price > 0),
    author VARCHAR(50) DEFAULT '未知',
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES BookCategory(id)
);

使用表连接查询所有图书的编号、图书名称、价格、所属类型名称：
SELECT bi.book_id, bi.name AS book_name, bi.price, bc.category_name AS category_name
FROM BookInfo bi
JOIN BookCategory bc ON bi.category_id = bc.id;

使用表连接查询计算机专业下的所有图书：
SELECT bi.book_id, bi.name AS book_name, bi.price, bc.category_name AS category_name
FROM BookInfo bi
JOIN BookCategory bc ON bi.category_id = bc.id
WHERE bc.category_name = '计算机';


将《Java语言基础》书的售价改为45元：
UPDATE BookInfo
SET price = 45
WHERE name = 'Java语言基础';

删除“网络安全”分类：
DELETE FROM BookCategory
WHERE category_name = '网络安全';