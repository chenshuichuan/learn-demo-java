package com.ricardo.ffmpeg.audio;

import io.github.mzdluo123.silk4j.AudioUtils;

import java.io.File;
import java.io.IOException;

public class Silk4jTest {
    public static void main(String[] args) {
        textToAudio();
    }
    public static File textToAudio(){
        // 原始MP3文件路径
        String mp3FilePath = "data/1698724715493.mp3";
        File mp3File = new File(mp3FilePath);
        try {
            AudioUtils.init(new File("data/voice"));
            return AudioUtils.mp3ToSilk(mp3File);
        } catch (IOException e) {
        }
        return null;
    }
}
