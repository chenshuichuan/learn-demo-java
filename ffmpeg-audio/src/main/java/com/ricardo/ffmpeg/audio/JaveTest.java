package com.ricardo.ffmpeg.audio;

import it.sauronsoftware.jave.AudioUtils;

import java.io.File;

public class JaveTest {
    public static void main(String[] args) {
        // 原始MP3文件路径
        String mp3FilePath = "data/1698724715493.mp3";
        // 日标SILK文件输出路径
        String silkFilePath ="data/output.amr";
        //String silkFilePath = "data/output.pcm";

        File sourceFile = new File(mp3FilePath);//输入
        File targetFile = new File(silkFilePath);//输出
        AudioUtils.amrToMp3(sourceFile, targetFile);//转换

    }

}
